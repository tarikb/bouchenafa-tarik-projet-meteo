<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Data;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DataController
 * @package App\Controller
 * @Route("/data")
 */
class DataController extends Controller {

    /**
    * @Route("/show", name="show")
    */
    public function show()
    {
        $datas = $this->getDoctrine()
            ->getRepository(Data::class)
            ->findAll();

        return $this->render('data/index.html.twig', [
            'datas' => $datas,
        ]);
    }

    /**
     * @Route("/new", name="new")
     */
    public function new()
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: index(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();

        $data = new data();
        $data->setStation('Paris');
        $data->setDate('21/01/2018');
        $data->setTemperature(12,2);
        $data->setWindDirection(20);
        $data->setWindSpeed(22,5);
        $data->setHumidity(12);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($data);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response('Nouveau produit ajouté:  '.$data->getStation() .$data->getDate() .$data->getTemperature() .$data->getWindDirection() .$data->getWindSpeed() .$data->getHumidity());
    }

    /**
     * @Route("/data/update", name="update")
     */
    public function update()
    {
        return $this->render('data/index.html.twig', [
            'controller_name' => 'DataController',
        ]);
    }


    /**
     * @Route("/delete", name="delete")
     */
    public function delete()
    {
        return $this->render('data/index.html.twig', [
            'controller_name' => 'DataController',
        ]);
    }
}
